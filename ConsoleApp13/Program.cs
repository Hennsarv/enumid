﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp13
{

    enum Mast { Risti=1, Ruutu, Ärtu, Poti}

    enum Kaart { Kaks=2, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss}

    [Flags]
    enum Tunnus {  Pole = 0, Puust = 1, Punane = 2, Suur = 4, Kolisev = 8, Imelik = 5}

    class Program
    {
        static void Main(string[] args)
        {
            Mast m = (Mast)3;
            Console.WriteLine(m);

            Kaart k = Kaart.Kuningas;
            Console.WriteLine((int)k);

            Tunnus t = Tunnus.Puust | Tunnus.Punane;

            Console.WriteLine(t);  // Puust, Punane
            t |= Tunnus.Suur;

            Console.WriteLine(t);  // Puust, Punane, Suur
            Console.WriteLine((int)t);  // 7

            t ^= Tunnus.Puust;
            Console.WriteLine(t);  // Punane, Suur

            Console.WriteLine((Tunnus)15);  // Puust, Kolisev    

            Console.WriteLine(Tunnus.Imelik ^ Tunnus.Puust);

            foreach (var x in Enum.GetNames(typeof(Tunnus)))
            {
                Console.WriteLine(x);
            }


            Kaart k1 = Kaart.Kuningas;
            Kaart k2 = Kaart.Emand;

            //if (k1 > k2)
            //    Console.WriteLine("Kuningas on kangem kui emand");
            //else
            //    Console.WriteLine("Emand on kõvem kui kuningas");


            Console.WriteLine($"{k1} {(k1 > k2 ? "tugevam" : "nõrgem")} kui {k2}");


            //if (
            //MessageBox.Show("Tere", 
            //    "Pealkiri", MessageBoxButtons.OKCancel, MessageBoxIcon.Error )
            //== DialogResult.OK)
            //{
            //    // ok teeme edasi
            //    Console.WriteLine("Teeme edasi");
            //}
            //else
            //{
            //    // enam ei tee
            //    Console.WriteLine("Lõpetame");
            //}
        }
    }
}
